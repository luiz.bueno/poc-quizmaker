// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    typescript: {
        typeCheck: true,
    },
    css: ['normalize.css'],
    devtools: { enabled: false },
    modules: [[
        '@nuxtjs/google-fonts',
        {
            families: {
                'Noto Sans': {
                    wght: '200..900',
                    ital: '200..700',
                },
            },
        },
    ], '@nuxt/image', "@nuxt/eslint"],
});
