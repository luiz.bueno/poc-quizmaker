export interface QuizOption {
    id: number;
    position?: number;
    label: string;
}
