import type { QuizOption } from "./QuizOption";
import type { QuizQuestion } from "./QuizQuestion";
import type { QuizEndForm } from "./QuizEndForm";

export interface Quiz {
    id: number;
    title: string;
    subtitle: string;
    questions: QuizQuestion[];
    endForm: QuizEndForm;
}
