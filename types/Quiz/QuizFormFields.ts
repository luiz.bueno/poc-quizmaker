export interface QuizFormFields {
    id: number;
    type: string;
    pattern?: string;
    position: number;
    label: string;
    placeholder: string;
}
