import type { QuizOption } from "./QuizOption";

export interface QuizQuestion {
    id: number;
    position: number;
    title: string;
    subtitle: string;
    options?: QuizOption[];
}
