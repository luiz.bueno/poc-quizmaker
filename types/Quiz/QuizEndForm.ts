import type { QuizFormFields } from "./QuizFormFields";

export interface QuizEndForm {
    subtitle: string;
    title: string;
    call: string;
    fields: QuizFormFields[];
    button: string;
}
