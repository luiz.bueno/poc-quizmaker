import { ref } from 'vue';
import type { Quiz } from "@/types/Quiz/Quiz";

export const useBoxedQuiz = (quiz: Quiz) => {
    const currentQuestionIndex = ref(0);

    const selectOption = (event: Event, optionId: number, questionId: number) => {
        event.preventDefault();
        const radioButton = document.getElementById(
            `question-${questionId}-option-${optionId}`
        ) as HTMLInputElement;
        if (radioButton) {
            radioButton.checked = true;
            currentQuestionIndex.value++;
        }
    };

    const backToPreviousQuestion = () => {
        if (currentQuestionIndex.value > 0) {
            currentQuestionIndex.value--;
        }
    };

    return {
        currentQuestionIndex,
        selectOption,
        backToPreviousQuestion
    };
};
